## Datasur Spyder

### Instalación
Clonar el repositorio  
```
git clone https://gitlab.com/robertomonges/datasur_spyder.git
```
Crear un entorno virtual.
```
virtualenv .ve
```
Activar el entorno virtual
```
source .ve/bin/activate
```
Instalar los paquetes en el entorno virtual
```
pip install -r requeriments.txt
```
Editar el archivo main.py y agregar los siguientes parámetros:
- usuario_datasur
- password_datasur
- anhos
- meses

Ejecutra el spyder
```
python main.py
```