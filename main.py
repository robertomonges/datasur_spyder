import http.client
from bs4 import BeautifulSoup
import json
import urllib.parse
import pandas as pd
import datetime
import os.path

# iniciamos spyder Datasur
begin_time = datetime.datetime.now()
print("Inicia spyder ", begin_time)

def urlencode(str):
    return urllib.parse.quote(str)

# lugar de descarga
carpeta_raiz = os.getcwd()
carpeta_descarga = carpeta_raiz + '/descargas'

## editar parametros antes de ejecutar
usuario_datasur = "user_example"
password_datasur = "pass_example"

anhos = ["2021"]
meses = ["02"]

vias = pd.read_csv(carpeta_raiz + '/filtros/viatransporte.txt', sep=';',keep_default_na=False)["viatransporte"]
paises = pd.read_csv(carpeta_raiz + '/filtros/paises.txt', sep=';' , keep_default_na=False)["paises"]
## fin parametros a editar


# creamos carpeta descargas si no existe
try:
    os.mkdir(carpeta_descarga)
except OSError:
    print("Ya existe %s, no hace falta crear directorio " % carpeta_descarga)
else:
    print("Se ha creado la carpeta: %s " % carpeta_descarga)

# obtenemos ip publica del cliente
conn = http.client.HTTPSConnection("ifconfig.me")
conn.request("GET", "/")
res = conn.getresponse()
data = res.read()
ip = str(data).replace("b", "").replace("'", "")




# parametros request 1 - login
conn1 = http.client.HTTPSConnection("datasur.com")
payload1 = 'data%5Bpage%5D=login&data%5Blogin_post%5D%5Bds_username%5D='+urlencode(usuario_datasur)+'&data%5Blogin_post%5D%5Bds_password%5D='+urlencode(password_datasur)+'&lang=es&ip='+ip
headers1 = {
  'Content-Type': 'application/x-www-form-urlencoded'
}


# enviamos request 1 - login
conn1.request("POST", "/website/seekers/pages.php", payload1, headers1)
res1 = conn1.getresponse()
data1 = res1.read()
print("Se inicia sesion")

# parseamos response html
soup1 = BeautifulSoup(data1, 'html.parser')

# obtenemos el token de sesion
#print(soup1)
tag = 0
for link in soup1.find_all('script'):
    tag += 1
    if tag == 3:
        n_linea = 0
        for linea in str(link).split('\n'):
            n_linea += 1
            if n_linea == 12:
                token = json.loads(linea.replace("var data = ", "").replace(";", "").replace("'", '"'))["token"]
                user_id = json.loads(linea.replace("var data = ", "").replace(";", "").replace("'", '"'))["user_id"]
                print("Se obtuvo el token: " + token)

# parametros request 2 - elegir pais
conn2 = http.client.HTTPSConnection("datasur.com")
payload2 = 'data%5Bpage%5D=seeker&data%5Bsearch%5D=4&lang=es&ip='+ip
headers2 = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

# enviamos request 2 - elegir pais
conn2.request("POST", "/website/seekers/pages.php", payload2, headers2)
res2 = conn2.getresponse()
data2 = res2.read()
#print(data.decode("utf-8"))
print("Pais elegido Paraguay")

reanudar_desde = 0

cant_request = 0
cant_archivo = 0
combinacion =0


print("Reanudar desde combinacion: " + str(reanudar_desde))
for pais in paises.index:
  for via in vias.index:
    for mes in meses:
      for anho in anhos:
        combinacion += 1 # contador de cobinaciones que se dan
        
        # verificamos si existe un parametro de reanudacion
        if combinacion >= reanudar_desde:
            nombre_archivo = 'anho_' + anho + '_mes_' + mes + '_via_' + via + '_pais_' + pais + '_.xlsx'
            
            print("-------------------------------NUEVA BUSQUEDA-------------------------------------")
            print("Combinacion: " + str(combinacion) + " - request: " + str(cant_request) + " - archivo: " +nombre_archivo)
            
            
            
            # si no existe el archivo se hace request, sino se salta
            if not os.path.isfile(carpeta_descarga + '/' + nombre_archivo):
                cant_request+=1 # se cuenta un request de busqueda
            
                print("Request nro. " + str(cant_request))
                print("buscando pais " + pais + " via " + via + " mes " + mes)
                # parametros request 3 - buscar
                conn3 = http.client.HTTPSConnection("core.datasur.com")
                payload3 = 'action=search&fields%5Boper%5D=im&fields%5Banio%5D=' + anho + '&fields%5Binimon%5D=' + mes + '&fields%5Bfinmon%5D=' + mes + '&fields%5Binp_43%5D=' + urlencode(via) + '&fields%5Binp_45%5D=' + urlencode(pais) + '&fields%5Bseek_id%5D=4&fields%5Btoken%5D=' + urlencode(token) + '&fields%5Buser_id%5D='+str(user_id)+'&fields%5Bcustom_view_id%5D=0&lang=es&ip=' + ip
                headers3 = {
                'User-Agent': 'Apache-HttpClient/4.3.3 (java 1.5)',
                'Content-Type': 'application/x-www-form-urlencoded'
                }

                # enviamos request 3 - buscar
                conn3.request("POST", "/seekers/api/", payload3, headers3)
                res3 = conn3.getresponse()
                data3 = res3.read()

                #  obtenemos el id de la busqueda
                search_id = str(json.loads(data3.decode("utf-8"))["search_id"])
                print("ID de busqueda " + search_id)

                # obtenemos la cantidad de registros en resultado
                search_result = str(json.loads(data3.decode("utf-8"))["search_result"])
                print("Cantidad de resultados " + search_result)

                if str(search_result) != '0':
                    # parametros request 4 - validar funcion exportar
                    conn4 = http.client.HTTPSConnection("core.datasur.com")
                    payload4 = 'action=export&user_id='+str(user_id)+'&token=' + urlencode(token) + '&rsp=false&lang=es&ip=' + ip
                    headers4 = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                    }

                    # enviamos request 4 - validar funcion exportar
                    conn4.request("POST", "/seekers/api/", payload4, headers4)
                    res4 = conn4.getresponse()
                    data4 = res4.read()
                    print("Validar funcion exportar: " + data4.decode("utf-8"))

                    # parametros request 5 - validar token de sesion
                    conn5 = http.client.HTTPSConnection("core.datasur.com")
                    payload5 = 'action=verifytoken&user_id='+str(user_id)+'&token=' + urlencode(token) + '&ip=' + ip
                    headers5 = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                    }

                    #  enviamos request 5 - validar token de sesion
                    conn5.request("POST", "/seekers/api/", payload5, headers5)
                    res5 = conn5.getresponse()
                    data5 = res5.read()
                    print("Validar token de session: " + data5.decode("utf-8"))

                    # parametros request 6 - descargar archvio
                    conn6 = http.client.HTTPSConnection("core.datasur.com")
                    payload6 = 'action=export&search_id=' + str(search_id) + '&format=xlsx&lang=es&token=' + urlencode(token) + '&user_id='+str(user_id)+''
                    headers6 = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                    }

                    # enviar request 6 - descargar archivo
                    conn6.request("POST", "/seekers/api/", payload6, headers6)
                    res6 = conn6.getresponse()
                    data6 = res6.read()
                    # print(data.decode("utf-8"))

                    cant_archivo += 1
                    with open(carpeta_descarga +'/'+ nombre_archivo, 'wb') as f:
                        f.write(data6)
                        print("Archivo guardado: " + nombre_archivo)
            else:
                print("Ya existe: " + nombre_archivo)


end_time = datetime.datetime.now()
print("Finaliza spyder ", end_time)
print("Tiempo de ejecución: " + str(end_time - begin_time))
print("Archivos descargados: " + str(cant_archivo))
print("Requests hechos: " + str(cant_request))
